extends Area3D

signal on_checkpoint_passed


func get_reset_point() -> Transform3D:
	return $ResetPoint.global_transform


func _on_body_entered(_body):
	emit_signal("on_checkpoint_passed")
