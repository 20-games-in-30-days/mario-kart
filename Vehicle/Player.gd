extends Node3D

@onready var ball: RigidBody3D = $Ball
@onready var car: Node3D = $Car
@onready var ground_ray: RayCast3D = $Car/Ray

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity: float = ProjectSettings.get_setting("physics/3d/default_gravity")

@export var sphere_offset: Vector3 = Vector3(0, -0.19, 0)
@export var acceleration: float = 500.0
@export var steering: float = 21.0
@export var turn_speed: float = 5.0
@export var turn_stop_limit: float = 0.25
@export var sideways_friction: float = 300.0


func _enter_tree() -> void:
	$TireTrails.markers = [$Car/WheelFL/Trail, $Car/WheelFR/Trail, $Car/WheelBL/Trail, $Car/WheelBR/Trail]
	$TireTrails.ground_ray = $Car/Ray


func _ready() -> void:
	ground_ray.add_exception(ball)


func snap_position(t: Transform3D) -> void:
	ball.global_transform = t
	car.global_transform = t
	ball.linear_velocity = Vector3.ZERO
	ball.angular_velocity = Vector3.ZERO


func _physics_process(delta: float) -> void:
	# apply sideways friction	
	var linear_velocity: Vector3 = ball.get_linear_velocity()
	var local_linear_velocity: Vector3 = car.global_transform.basis.inverse() * linear_velocity
	
	if ground_ray.is_colliding():
		var input: Vector2 = Input.get_vector("left", "right", "gas", "brake")
		
		ball.apply_central_force(car.global_transform.basis.z * input.y * acceleration)
		
		if ball.linear_velocity.length() > turn_stop_limit:
			var new_basis: Basis = car.global_transform.basis.rotated(car.global_transform.basis.y, -input.x).orthonormalized()
			var turn_direction: float = -sign(local_linear_velocity.z)
			car.global_transform.basis = car.global_transform.basis.slerp(new_basis, turn_speed * turn_direction * delta)
			car.global_transform = car.global_transform.orthonormalized()
		
		var n: Vector3 = ground_ray.get_collision_normal()
		var xform: Transform3D = align_with_y(car.global_transform, n.normalized() )
		car.global_transform = car.global_transform.interpolate_with(xform, 10.0 * delta)
	
		var sideways_velocity: float = local_linear_velocity.x
		var sideways_force: Vector3 = sideways_velocity * sideways_friction * -car.transform.basis.x
		ball.apply_central_force(sideways_force)
	
	car.transform.origin = ball.transform.origin + sphere_offset


func align_with_y(xform: Transform3D, new_y: Vector3) -> Transform3D:
	xform.basis.y = new_y
	xform.basis.x = -xform.basis.z.cross(new_y).normalized()
	xform.basis = xform.basis.orthonormalized()
	return xform
