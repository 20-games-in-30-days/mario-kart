extends Node3D

# Maximum number of points in the trail
@export var max_points: int = 100
@export var trail_step: float = 0.4

var trails
var markers
var ground_ray: RayCast3D
var last_pos = [Vector3.ZERO, Vector3.ZERO, Vector3.ZERO, Vector3.ZERO]
var airborne := false


func _enter_tree():
	trails = [$FL/Trail, $FR/Trail, $BL/Trail, $BR/Trail]
	assert(trails.size() == markers.size())


func _process(_delta):
	# Add the current position to the trail
	if ground_ray.is_colliding():
		for i in markers.size():
			if airborne:
				trails[i].curve.clear_points()
			
			var trail_pos: Vector3 = markers[i].global_transform.origin - (2 * global_transform.origin)
			# Bail if we're too small a distance.
			if (last_pos[i] - trail_pos).length() < trail_step:
				continue
			
			last_pos[i] = trail_pos
			trails[i].curve.add_point(trail_pos)

			# Remove old points if the trail has more than max_points
			if trails[i].curve.get_point_count() > max_points:
				trails[i].curve.remove_point(0)
		airborne = false
	
	else:
		airborne = true
