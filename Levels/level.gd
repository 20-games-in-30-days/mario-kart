extends Node3D

signal lap_completed(laps: int)


func _on_checkpoints_lap_completed():
	emit_signal("lap_completed")
