extends Node

var lap_times = []
var current_lap_start_time = 0
var current_lap_end_time = 0
var lap_number = 0  # Start from 0th lap

@onready var player = $Player
@onready var start_pos = $Level/Player_Start
@onready var checkpoint = $Level/Checkpoints
@onready var laps = $UI/Laps
@onready var time = $UI/Time
@onready var finish = $UI/Finish


func _ready():
	player.global_position = start_pos.global_position
	player.rotation = start_pos.global_rotation


func _on_fallout_body_entered(_body):
	var transform: Transform3D = checkpoint.get_reset_position()
	player.snap_position(transform)


func _process(delta):
	if (lap_number > 0) and (lap_number < 4):
		# update the current lap time
		current_lap_end_time += delta
		var lap_time_str = "Lap " + str(lap_number) + ": " + str(format_time(current_lap_end_time - current_lap_start_time))

		# update the previous lap times
		if len(lap_times) > 0:
			var lap_iter = range(len(lap_times))
			lap_iter.reverse()
			for i in lap_iter:
				lap_time_str += "\nLap " + str(i+1) + ": " + str(format_time(lap_times[i]))

		time.text = lap_time_str


func _on_level_lap_completed():
	lap_number += 1

	# if all laps completed, print "Race Complete!"
	if lap_number == 4:
		lap_times.append(current_lap_end_time - current_lap_start_time)
		finish.text = "Race Complete!\nYour time: " + str(format_time(lap_times.reduce(func(accum, number): return accum + number)))
		finish.visible = true  # Show the Finish UI element
	
	elif lap_number > 0:
		if lap_number > 1:
			# add the current lap time to the lap_times array
			lap_times.append(current_lap_end_time - current_lap_start_time)

		# start tracking a new lap
		current_lap_start_time = current_lap_end_time

		# update the lap count text
		laps.text = "  Laps: " + str(lap_number) + "/3"


func format_time(t):
	# convert the time to minutes and seconds
	@warning_ignore("integer_division")
	var minutes = int(t) / 60
	var seconds = int(t) % 60

	# format the time string as "M:SS"
	return str(minutes) + ":" + str(seconds).lpad(2, "0") + "  "
