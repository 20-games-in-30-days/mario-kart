extends Node3D

signal lap_completed(laps: int)

@onready var checkpoint_1 = $Checkpoint1
@onready var checkpoint_2 = $Checkpoint2
@onready var checkpoint_3 = $Checkpoint3
@onready var checkpoint_4 = $Checkpoint4
@onready var checkpoint_5 = $Checkpoint5

var current_checkpoint = 1
var last_position: Transform3D = Transform3D.IDENTITY


# Called when the node enters the scene tree for the first time.
func _ready():
	checkpoint_1.connect("on_checkpoint_passed", on_checkpoint_passed.bind(1))
	checkpoint_2.connect("on_checkpoint_passed", on_checkpoint_passed.bind(2))
	checkpoint_3.connect("on_checkpoint_passed", on_checkpoint_passed.bind(3))
	checkpoint_4.connect("on_checkpoint_passed", on_checkpoint_passed.bind(4))
	checkpoint_5.connect("on_checkpoint_passed", on_checkpoint_passed.bind(5))

# Called when the player passes a checkpoint.
func on_checkpoint_passed(checkpoint_number):
	if checkpoint_number == current_checkpoint:
		if current_checkpoint == 5:
			current_checkpoint = 1
		else:
			if current_checkpoint == 1:
				emit_signal("lap_completed")
			current_checkpoint += 1
		
		match checkpoint_number:
			1: last_position = checkpoint_1.get_reset_point()
			2: last_position = checkpoint_2.get_reset_point()
			3: last_position = checkpoint_3.get_reset_point()
			4: last_position = checkpoint_4.get_reset_point()
			5: last_position = checkpoint_5.get_reset_point()


# Returns the value of get_reset_point for the checkpoint that was passed most recently.
func get_reset_position() -> Transform3D:
	return last_position
