extends Control


func _on_test_track_pressed():
	get_tree().change_scene_to_file("res://Levels/game_testlevel.tscn")


func _on_real_track_pressed():
	get_tree().change_scene_to_file("res://Levels/game_finallevel.tscn")
